# ChronSys -- Repository for descriptions of Chronological Systems

**migrated to [https://codeberg.org/OliverNakoinz/chronsys](https://codeberg.org/OliverNakoinz/chronsys)**

## Description

This repository contains descriptions of relative chronological systems used in archaeology. The purpose of this collection is to provide a reference for chronological information. Each chronological system is represented by a csv-file containing the different phases as rows. This file comprises information on the name of the phases, the relationship of the different phases, absolute dating and a unique chronology string (chronstring) that is used to refer to chronological units of specific chronological systems. A metadata yaml file according to dcmi, accompanies each csv file.

### Chrono csv files

- `c_system`: name of the chronology system
- `id`: identification number inside the chronology system
- `p_id`: id of the superior entity
- `c_level`: hierarchy level starting with the most general entity
- `c_order`: order inside one hierarchy level
- `datstring`: chronology string (chronstring)
- `name_short`: short name, must be identical with the last phase name of the chronology string, avoid tailing spaces
- `name_long`: more comprehensive name of the phase
- `start`: start year of the phase, negative means BC, same year as end year of the previous phase
- `end`: end year of the phase


### Chronstring

The key component of the concept is the chronology string. 
A chronstring, for instance `fortME_Vorg>BZ>MBZ>BzC_0.8`, comprises three parts, delimited by an underscore:

- chronology system: `fortME` (also used as file names of the csv and xxxx files)
- dating string: `Vorg>BZ>MBZ>BzC`
- probability: `0.8`

The dating string as the core component of the chronostring concatenates the abbreviations of the different hierarchical levels of the phases, separated by ">". Due to the construction scheme of the chronology string the parts of the chronology string, phase names and chronology system name must **not** include 

- spaces, 
- underscores
- ">" symbols
    
The chronology string should always use utf-8 coding and hence, special characters should work but should be used carefully and with caution. 

For radiocarbon dates chronstrings also are defined but obviously not included in this repository. A radiocarbon chronostring, for instance `14C_Poz-16047_3510_30`, comprises four parts, delimited by an underscore:

- keyword: `14C`
- Lab code and lab number: `Poz-16047`
- date BP: `3510`
- standard deviation: `30`

Other projects that compile archaeological chronologies, but provide different formats are:

- [chronontology](https://chronontology.dainst.org)
- [Periodo](https://perio.do/en/)

## Usage

Chronstrings are stored in character/text columns and can be concatenated, with the individual chronstrings separated by a comma:

`fortME_Vorg>EZ>Lt>FLT>LtA_1.0, fortME_Vorg>EZ>Lt>FLT>LtA1_0.5, fortME_Vorg>EZ>Lt>FLT>LtB2_0.5`

The R-package [archChron](https://gitlab.com/oliver.nakoinz/archchron) aims to provide functions for working with chronstrings.


## Roadmap

- including more chronological systems
- define chronstring for dendrochronology
- complete citations in metadata

## Contributing

Contributions of further chronological systems are welcome.


## Authors and acknowledgement

### Authors

- Oliver Nakoinz
- xxx

### Acknowledgement

This concept of chronological references is derived from the [Kiel Fortification database](https://gitlab.com/oliver.nakoinz/fortification_int) 

### Use of chronological references in other projects

- [KFD](https://gitlab.com/oliver.nakoinz/fortification_int)
- [archChron](https://gitlab.com/oliver.nakoinz/archchron)
- [simple Data Base](https://gitlab.com/oliver.nakoinz/sDB)

## License

https://creativecommons.org/licenses/by/4.0/legalcode

