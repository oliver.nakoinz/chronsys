
#' `rDownloadChronsys` downloads and loads the list of chronological system files.
#'
#' The Function tests if the .RData with the chronological system definitions exists and if it exists if all requested systems are included. 
#' If not the .csvs downloaded from gitlab or an individual source folder. 
#'
#' @param csys   chr, vector of the names of chronological systems to load.
#' @param source chr, "gitlab" for standard-gitlab-repository or a valid path ending with "/" to the folder with the .csv files.
#' @param path   chr, path for the .RData file with chronological system definition list
#' @param force  logical, if true the chronological definitions are loaded from source even if the .RData exists and is complete.
#'
#' @return list of chronological systems
#' @export
#'
#' @examples
rDownloadChronsys <- function(
        csys   = c("fortME", "fortNE"),
        source = "gitlab",
        path   = "./",
        force  = F
    ){
        complete <- F
        rmd_file <- paste0(path,"chronsystems.RData")
        if (file.exists(rmd_file) | !isTRUE(force)){
            load(rmd_file)
            complete <- min(csys %in% names(csystems)) == 1
            }
        if (isFALSE(complete)){
            if (source == "gitlab"){
                source1  <- "https://gitlab.com/oliver.nakoinz/chronsys/-/raw/main/"
                source2  <- ".csv?inline=false"
            }else{
                source1  <- source
                source2  <- ".csv"
            }
        csystems <- list()
        for (sys in csys){
            gitlab   <- paste0(source1, sys, source1)
            chronsys <- read.csv(
                file   = gitlab,
                header = TRUE,
                sep    = ";",
                dec    = ".",
                stringsAsFactors = FALSE
            )[,1:10] # exclude additional comments
            csystems <- append(csystems, list(chronsys))
        }
        names(csystems) <- csys
        save(csystems,
             file = rmd_file
            )
        }
        return(csystems)
    }

#csystems[[1]]
#csystems$fortME

